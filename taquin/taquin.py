#! /usr/bin/python3

"""Ce module définit la classe Taquin et des exemples de taquin."""

# dimensions des taquins
NBLIG=4
NBCOL=4

# Les éléments du taquin sont numérotés de 1 à NBLIG*NBCOL.
# Représentation du trou :
VIDE=0

class Taquin():
    """Classe représentant une position du taquin"""

    def voisins(self):
        """Renvoie toutes les grilles qu'on peut obtenir à partir de 
        celle-ci en un déplacement."""
        res = []
        for di, dj in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
            if 0 <= self.it+di < NBLIG and 0 <= self.jt+dj < NBCOL:
                res.append(self.mouvement(di, dj))
        return res

    def est_resolu(self):
        """Renvoie un booléen disant si le taquin est résolu ou non."""
        return self == Taquin()

    def __init__(self,
        config=[[(NBCOL*i + j + 1) % (NBCOL*NBLIG)
                     for j in range(NBCOL)] for i in range(NBLIG)]):
        """Construit un nouveau taquin. Si config n'est pas donné,
        construit le taquin en forme résolue"""
        self.grille = tuple(tuple(config[i][:NBCOL]) for i in range(NBLIG))
        nbtrous=0
        for i in range(NBCOL):
            for j in range(NBLIG):
                if config[i][j] == VIDE:
                    nbtrous += 1
                    self.it = i
                    self.jt = j
        assert nbtrous == 1

    def __repr__(self):
        return 'Taquin('+repr(self.grille)+')'

    def __eq__(self, other):
        return self.grille == other.grille

    def __hash__(self):
        return hash(self.grille)

    def mouvement(self, di, dj):
        """Renvoie la grille obtenue en échangeant le trou,
        situé en position (i, j) avec l'élément situé en position
        (i+di, j+dj).
        Précondition : cette position doit exister"""
        g = [ list(l) for l in self.grille]
        i = self.it + di
        j = self.jt + dj
        g[self.it][self.jt] = g[i][j]
        g[i][j] = 0
        return Taquin(g)

# Le taquin solution :
solution = Taquin()

# Exemples de taquins à résoudre :
exemples = [
  Taquin(
    [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]),
  Taquin(
    [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 0], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 3, 4], [5, 6, 7, 0], [9, 10, 11, 8], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 3, 4], [5, 6, 0, 7], [9, 10, 11, 8], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 0, 4], [5, 6, 3, 7], [9, 10, 11, 8], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 0], [5, 6, 3, 7], [9, 10, 11, 8], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [5, 6, 3, 0], [9, 10, 11, 8], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [5, 6, 3, 8], [9, 10, 11, 0], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [5, 6, 3, 8], [9, 10, 0, 11], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [5, 6, 3, 8], [9, 0, 10, 11], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [5, 0, 3, 8], [9, 6, 10, 11], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [0, 5, 3, 8], [9, 6, 10, 11], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [0, 6, 10, 11], [13, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [13, 6, 10, 11], [0, 14, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [13, 6, 10, 11], [14, 0, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [13, 0, 10, 11], [14, 6, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [0, 13, 10, 11], [14, 6, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [14, 13, 10, 11], [0, 6, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [14, 13, 10, 11], [6, 0, 15, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [14, 13, 10, 11], [6, 15, 0, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 3, 8], [14, 13, 0, 11], [6, 15, 10, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 0, 8], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[1, 2, 4, 7], [9, 5, 8, 0], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[1, 2, 4, 0], [9, 5, 8, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[1, 2, 0, 4], [9, 5, 8, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[1, 0, 2, 4], [9, 5, 8, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[0, 1, 2, 4], [9, 5, 8, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[9, 1, 2, 4], [0, 5, 8, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[9, 1, 2, 4], [5, 0, 8, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[9, 0, 2, 4], [5, 1, 8, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[9, 2, 0, 4], [5, 1, 8, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 0, 7], [14, 13, 3, 11], [6, 15, 10, 12]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 3, 7], [14, 13, 0, 11], [6, 15, 10, 12]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 3, 7], [14, 13, 11, 0], [6, 15, 10, 12]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 3, 7], [14, 13, 11, 12], [6, 15, 10, 0]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 3, 7], [14, 13, 11, 12], [6, 15, 0, 10]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 3, 7], [14, 13, 0, 12], [6, 15, 11, 10]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 3, 7], [14, 0, 13, 12], [6, 15, 11, 10]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 3, 7], [0, 14, 13, 12], [6, 15, 11, 10]]),
  Taquin(
    [[9, 2, 8, 4], [5, 1, 3, 7], [6, 14, 13, 12], [0, 15, 11, 10]])
    ]

# J'ai constaté sur le taquin suivant que le nombre de mouvements nécessaire est
# au moins égal à 56. J'ai trouvé une solution en 180 mouvements, ce qui n'est
# pas optimal (on peut montrer que 80 mouvements suffisent pour toute
# configuration).

difficile = Taquin(
    [[7, 12, 5, 11], [8, 3, 1, 14], [15, 9, 13, 2], [4, 0, 6, 10]])

# Le nombre de mouvements du taquin suivant est égal à 80 d'après la thèse de
# Ralph Udo Gasser, Harnessing Computational Resources, 1995, chap 6, section 5.
# Disponible sur
# https://pdfs.semanticscholar.org/25dd/f98704a27aa451dc5c7f75eedc07f5f3f614.pdf
# J'arrive à trouver une solution en 160 mouvements et à constater que le nombre
# de mouvements nécessaire est au moins égal à 70.

le_plus_difficile = Taquin(
    [[0,12,10,13],[15,11,14,9],[7,8,6,2],[4,3,5,1]]
    )
