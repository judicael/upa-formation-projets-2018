# On veut étant donné un taquin t, trouver un comment le résoudre.

# Pour modéliser un taquin, deux possibilités :
# 1) utiliser le module taquin fourni.
#    il s'agit alors de faire un
#    import taquin
#    qui fournit une classe Taquin.
#    Le taquin sous forme résolue est obtenu
#    par Taquin()
#    La variable exemples de ce module est un tableau d'exemples
#    tel que pour tout i < len(exemples), taquin.exemples[i] est
#    un taquin à distance i de la solution.
#    On pourra utiliser t.voisins() pour obtenir les configurations
#    voisines d'un taquin t, t.est_resolu() pour savoir si un taquin
#    est sous forme résolue et t.grille pour obtenir la
#    réprésenttation d'un taquin sous forme de tableau de tableaux
#    (la valeur d'une case étant alors 0 pour l'emplacement vide et
#    son numéro pour une tuile du taquin).
#
# 2) modéliser soi-même les taquins et écrire des fonctions
#    offrant des fonctionnalités similaires.
#
# Vous pouvez choisir l'une ou l'autre façon de faire suivant que
# vous voulez plutôt vous intéresser à la modélisation ou aux
# questions algorithmiques.

import taquin
from queue import Queue

# On dira qu'une liste [t0, ..., t(n-1)] est un chemin de t vers la
# solution si t0 = t et t(n-1).est_resolu() vaut vrai, et t0, ...,
# t(n-1) est une suite de taquins tels qu'on peut passer de t(i) à
# t(i+1) en un mouvement (pour i=0, ..., i=n-2).

# En particulier si on a t.est_resolu(), la liste [t] est un chemin
# vers la solution.

# On peut obtenir la liste des configurations voisines d'un taquin t
# par t.voisins()

def rech_largeur(t):
    """Effectue une recherche en largeur et retourne un chemin de t
    vers la solution, ou None s'il n'y en a pas."""
    # la file des taquins visités mais non encore traités,
    # accompagnée du chemin jusqu'à ceux-ci (on mettra les nouveaux
    # éléments à gauche et on les enlèvera à droite)
    q = Queue()
    q.put((t, [t]))
    # on marque t comme vu :
    vus = set([t])
    while not q.empty():
        x, ch = q.get()
        if x.est_resolu():
            return ch
        for v in x.voisins():
            if v not in vus:
                vus.add(v)
                q.put((v, ch + [v]))
    # la file est vide et on n'a rien trouvé :
    return None

# Sur mon PC, la recherche en largeur fonctionne pour
# exemple[18]. Pour exemple[19], python utilise plus de 1 Go de
# mémoire vive.

# Il est donc intéressant d'essayer d'autres stratégies de recherche.

# La recherche en profondeur consommerait sans doute autant de mémoire
# voire plus. De plus, elle ne retournerait probablement pas les
# chemins les plus courts.

# On va essayer un autre type de recherche en profondeur : on cherche
# en profondeur jusqu'à une profondeur bornée, sans mémoriser les
# configurations par lesquelles on est passé.

# Plus précisément, on écrira une fonction rech_chemin_borne(t, b, c)
# qui, pour t taquin, b entier et c un chemin supposer emmener à la
# configuration t, doit retourner un chemin c' de longueur au plus b,
# commençant par c arrivant à la solution.  Si aucun chemin de ce type
# n'existe, la fonction renvoie None. Attention la longueur du chemin c
# est len(c) - 1

#    L'implantation peut se faire récursivement de la façon suivante :
#    * si len(c) - 1 > b, on retourne None
#    * sinon, si t.est_resolu(), on retourne c.
#    * sinon, on essaie, pour chaque v voisin de t, d'exécuter
#      rech_chemin_borne(v, b, c+[v]) et si pour l'un au moins d'entre
#      eux le résultat n'est pas None, on le renvoie. Sinon, on
#      renvoie None.

def rech_chemin_borne(t, b, c):
    if len(c) - 1 > b:
        return None
    if t.est_resolu():
        return c
    for v in t.voisins():
        c.append(v)
        r = rech_chemin_borne(v, b, c)
        if r is not None:
            return r
        c.pop()
    return None


# On peut vérifier que rech_chemin_borne permet par exemple de
# résoudre jusqu'à exemples[12] (au prix d'un peu de patience
# cependant).
#
# Cette méthode comporte deux défauts majeurs :
#
# * On ne sait pas a priori à quelle profondeur chercher.
#
# * Si on cherche à une profondeur trop élevée, on ne trouve pas
#   toujours la meilleure solution (essayer sur des exemples).

def rech_par_approfondissement(t):
    """Cette recherche consiste à simplement à essayer
    rech_chemin_borne pour des valeurs successives de la borne sur la
    longueur du chemin égales successivement à 1, 2, ... jusqu'à
    trouver une solution."""
    b = 0
    ch = None
    while ch is None:
        b += 1
        print('Recherche à profondeur ' + str(b))
        ch = rech_chemin_borne(t, b, [])
    return ch

# On peut améliorer cette recherche en éliminant de notre recherche
# les chemins qui passent deux fois par le même sommet. Les chemins
# qui ne passent pas deux fois par le même sommet sont appelés des
# chemins simples.

def rech_chemin_simple_borne(t, b, c):
    """Pour t taquin, b entier et c un chemin simple supposé emmener
    à la configuration t, renvoie un chemin simple c' de longueur au
    plus b, commençant par c arrivant à la solution.
    Si aucun chemin de ce type n'existe, la fonction renvoie None."""
    if len(c) - 1 > b:
        return None
    if t.est_resolu():
        return c
    for v in t.voisins():
        if v not in c:
            c.append(v) # c reste un chemin simple
            r = rech_chemin_simple_borne(v, b, c)
            if r is not None:
                return r
            c.pop()
    return None

# Essayez votre fonction sur exemples[12], exemples[13] et
# exemples[16] et comparez avec les temps mis par rech_chemin_borne.

# Comme précédemment, on peut faire une recherche par
# approfondissement.

def rech_par_approfondissement2(t):
    """Effectue une recherche par approfondissement successifs en
    utilisant rech_chemin_simple_borne."""
    b = 0
    ch = None
    while ch is None:
        b += 1
        print('Recherche à profondeur ' + str(b))
        ch = rech_chemin_simple_borne(t, b, [])
    return ch

# Remarquez que rech_par_approfondissement2 donne les mêmes solutions
# que rech_par_approfondissement mais les donne plus vite.

# rech_chemin_simple_borne utilise en effet le fait que les chemins
# les plus courts sont nécessairement simples pour échouer plus vite
# dans ses recherches.

# Échouer rapidement, ou plus exactement, se rendre compte rapidement
# compte qu'on se fourvoie, est de manière générale un ingrédient
# essentiel des algorithmes de recherche.

# On peut donc chercher à échouer plus rapidement dans les requêtes
# précédentes. Une façon de faire est d'utiliser une fonction h(t) qui
# donnera une minoration du nombre de mouvements qu'il reste à
# parcourir pour trouver la solution en partant du taquin t.

# On va donc écrire ci-dessous deux fonctions h1 et h2 qui minorent ce
# nombre de mouvements.

def h1(t):
    """Renvoie le nombre de carreaux mal placés dans le taquin t."""
    g1 = t.grille
    g2 = taquin.Taquin().grille
    c = 0
    for i in range(taquin.NBLIG):
        for j in range(taquin.NBCOL):
            if g1[i][j] != taquin.VIDE and g1[i][j] != g2[i][j]:
                c += 1
    return c


def pos(v):
    """Renvoie la position normale du carreau de valeur v dans la
    grille."""
    return ((v - 1) // taquin.NBCOL, (v - 1) % taquin.NBCOL)

def h2(t):
    """Retourne la somme des distances de Manhattan des carreaux à
    leurs positions dans la solution."""
    g = t.grille
    dist = 0
    for i in range(taquin.NBLIG):
        for j in range(taquin.NBCOL):
            v = g[i][j]
            if v != taquin.VIDE:
                isol, jsol = pos(v)
                dist += abs(i - isol) + abs(j - jsol)
    return dist


# On veut maintenant écrire rech_chemin_borne_heuristique(h, t, b, c),
# qui doit trouver un chemin simple commençant par c, de longueur au
# plus b et arrivant à la solution.
#
# On implantera cette fonction comme précédemment, à deux différences
# près :
# * On échoue, lors du test initial, dès que len(c) - 1 + h(t) > b.
#
# * Si l'on échoue au moins une fois pour cette raison, on veut donner
#   une information disant quelle est la plus petite valeur de n +
#   h(t) qui dépassait b lors de la recherche. Cela sera utile pour
#   effectuer des recherches par approfondissement successifs.
#
# Pour cela, on ne retournera plus simplement un chemin ou None, mais
# un couple: on retournera (True, c) si on a trouvé un chemin c ; et
# (False, k) si on n'a pas trouvé de chemin et que k est la plus
# petite valeur de len(c) - 1 + h(t) qui dépassait b lors de la
# recherche (ou k = +infini si on n'a jamais échoué en raison du fait
# que len(c) - 1 + h(t) était supérieur à b).

INFINI = 10000

def rech_chemin_borne_heuristique(h, t, b, c):
    k = len(c) - 1 + h(t)
    if k > b:
        return (False, k)
    if t.est_resolu():
        return (True, c)
    kmin = INFINI
    for v in t.voisins():
        if v not in c:
            c.append(v) # c reste un chemin simple
            r, v = rech_chemin_borne_heuristique(h, v, b, c)
            if r:
                return (r, v)
            kmin = min(kmin, v)
            c.pop()
    return (False, kmin)


# Essayez cette fonction avec h1 et h2 pour résoudre des taquins.
# Regardez jusqu'à quelles profondeurs vous arrivez avec l'une et
# l'autre heuristique.

# Comme précédemment, on peut faire de l'approfondissement.
# L'algorithme ainsi obtenu s'appelle IDA*, d'où le nom de la fonction
# ci-dessous.

def ida_star(h, t):
    """Recherche un chemin vers la solution du taquin en partant de t
    par approfondissement.  Au lieu d'augmenter de un la profondeur de
    recherche à chaque appel, on va utiliser les informations sur
    l'échec pour savoir à quelle profondeur chercher : initialement,
    on cherche à profondeur 0 en utilisant la fonction
    rech_chemin_borne_heuristique. Si celle-ci retourne une solution,
    ida_star la retourne. Sinon, elle a retourné (False, k) ce qui
    signifie qu'il est nécessaire que la borne de recherche soit
    supérieure ou égale à k pour qu'on puisse explorer de nouveaux
    chemins. On va donc effectuer une recherche avec la nouvelle borne
    de recherche k. Si celle-ci réussit, on retourne le résultat,
    sinon elle retourne (False, k') et on retente une recherche avec
    la nouvelle borne k', etc."""
    k = 0
    r = False
    while not r:
        print('Recherche à profondeur ' + str(k))
        r, k = rech_chemin_borne_heuristique(h, t, k, [t])
    # r vaut True, donc k est le chemin cherché :
    return k
