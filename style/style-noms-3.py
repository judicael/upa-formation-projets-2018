def matrice_2():

    plateau = matrice_1() 

    PIECEABOUGER = [[7, 5], [5, 7], [5, 11], [7, 13], [11, 13], [13, 11], [13, 7], [11, 5]]

    for i in(PIECEABOUGER):

        abscisse = i[0]

        ordonnee = i[1]

        plateau[abscisse - 1, ordonnee], plateau[abscisse, ordonnee - 1] = plateau[abscisse, ordonnee - 1], plateau[abscisse - 1, ordonnee]

        plateau[abscisse, ordonnee + 1], plateau[abscisse + 1, ordonnee] = plateau[abscisse + 1, ordonnee], plateau[abscisse, ordonnee + 1]

    return plateau
