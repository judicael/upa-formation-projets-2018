def echanger_deux_bonbons(G, c1, c2):
    W = np.array(G)
    i1, j1 = c1
    i2, j2 = c2
    W[i1, j1], W[i2, j2] = W[i2, j2], W[i1, j1]
    if i1 == i2 and abs(j1 - j2) == 1:
        ...
