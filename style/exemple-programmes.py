def arret_etage(L): #L provient de etat_personne
    for i in range (len(L)):
        arrivee, voeu, position=L[i]
        if arrivee==etage and i!=0 : #si l'étage où est la personne coincide avec l'endroit où est l'ascenseur(qui correspondra à une autre fonction)
            temps+=20 #l'ascenseur s'arrête 20 unités de temps
            L[i][2]= dedans #on modifie la valeur de la position pour la personne i
        elif voeu==etage and L[i][2]==dedans: #si le voeu coincide avec l'endroit où est l'ascenseur et que la personne est dedans 
            temps+=20
            L[i][2]=sortie #la personne sort   
            L.pop(i) #la personne i est supprimée de la grande liste de toutes les personnes 
            
            
def tableau(n):
    T=[[0]*(2*n+1) for i in range(2*n+1)] # car besoin de 2*n valeurs pour modéliser le carré, on met 2*n+1 dans range car la dernière valeur n'est pas prise en compte
    for i in range (0,n+1): # car n+1 bords sur une colonne
        for k in range (0,n+1): # même raisons
            T[2*i][2*k]=2     # 2 = modélisation des angles
    for j in range(0,n):
        T[0][2*j+1] = 1
        T[2*j+1][0] = 1
        T[2*n][2*j+1] = 1
        T[2*j+1][2*n] = 1 # 1 = modélisation des traits des bords pour la matrice initiale
    return np.array(T)
    
def changement(T,l,c): # l =ligne,  c =colonne 
    T[l,c]=1    #on va changer un 0 en 1 afin de pouvoir tracer des traits pour former des carrés
          
          
def fonction_generale(n):
    joueur1 = 3 # valeurs affectées au carré complété par le joueur 1 ou 2
    joueur2 = 4
    j1 = 0
    j2 = 0 # carrés occupés par les joueurs 1 et 2
    traits_occupe=[]
    for j in range(n):
        traits_occupe.append((0,2*j+1))
        traits_occupe.append((2*j+1,0))
        traits_occupe.append((2*n,2*j+1))
        traits_occupe.append((2*j+1,2*n))
           
    T = tableau(n)
    tour=1
    while not(fin_partie(j1,j2,n)):
        if tour%2==0:
            marqueur = joueur2
            print('le joueur 2 joue')
        else:
            marqueur = joueur1
            print('le joueur 1 joue')
        (l,c) = convertirclics(n,traits_occupe,tour)
        while not(trait_valide(T,l,c,traits_occupe)): # tant que le trait n'est pas validé, on redemande des coordonées
            (l,c) = convertirclics(n,traits_occupe,tour)
        changement(T,l,c)
        #on marque le trait choisi dans la matrice
        traits_occupe.append((l,c)) #on met le trait choisi dans la liste des traits occupés
        nb_carre_gagne, liste_carre_complete = carre_complete(l,c,T,traits_occupe,marqueur)
        if nb_carre_gagne != 0: #on rejoue tant que le nombre de carrés complétés est non nul , le tour ne change pas
            if tour%2 == 0:
                j2+=nb_carre_gagne
                for i in range(int(len(liste_carre_complete))):
                    x,y = liste_carre_complete[i][0],liste_carre_complete[i][1] # on parcourt la liste des carrés complétés et on leur affecte la couleur relative au joueur qui les a remportés 
                    plt.scatter(x, y, c= 'red',s=1000,alpha=0.7, edgecolors='none')
            else:
                j1+=nb_carre_gagne
                for i in range(int(len(liste_carre_complete))):
                    x,y = liste_carre_complete[i][0],liste_carre_complete[i][1]
                    plt.scatter(x, y, c= 'blue',s=1000,alpha=0.7, edgecolors='none')
        else:
            tour+=1 # on change de joueur
    interface_graphique_gagnant(n,traits_occupe,tour,j1,j2)
    
    
#extrait de othello    
def directions(i,j,plateau) : #renvoie la liste des numeros sur le plateau à etudier a partir du point que l'on veut positionner dans chaque direction) #ok

    couleur_direction1 = [plateau[(i-k),j]for k in range(1,2*n) if (i-k) >= 0]

    index_direction1 = [[i-k,j]for k in range(1,2*n) if (i-k) >= 0]

    

    couleur_direction2 = [plateau[i-k,j+k] for k in range(1,2*n) if (i-k) >= 0 and (j+k) <= 7]

    index_direction2 = [[i-k,j+k] for k in range(1,2*n) if (i-k) >= 0 and (j+k) <= 7]

    

    couleur_direction3 = [plateau[i,j+k] for k in range(1,2*n) if (j+k) <= 7]

    index_direction3 = [[i,j+k] for k in range(1,2*n) if (j+k) <= 7]

    

    couleur_direction4 = [plateau[i+k,j+k] for k in range(1,2*n) if (i+k) <= 7 and (j+k) <= 7]

    index_direction4 = [[i+k,j+k] for k in range(1,2*n) if (i+k) <= 7 and (j+k) <= 7]

    

    couleur_direction5 = [plateau[i+k,j] for k in range(1,2*n) if (i+k) <= 7]

    index_direction5  = [[i+k,j] for k in range(1,2*n) if (i+k) <= 7]

    

    couleur_direction6 = [plateau[i+k,j-k] for k in range(1,2*n) if (i+k) <= 7 and (j-k) >= 0]

    index_direction6 = [[i+k,j-k] for k in range(1,2*n) if (i+k) <= 7 and (j-k) >= 0]

    

    couleur_direction7 = [plateau[i,j-k] for k in range(1,2*n) if (j-k) >= 0]

    index_direction7 = [[i,j-k] for k in range(1,2*n) if (j-k) >= 0]

    

    couleur_direction8 = [plateau[i-k,j-k] for k in range(1,2*n) if (i-k) >= 0 and (j-k) >= 0]

    index_direction8 = [[i-k,j-k] for k in range(1,2*n) if (i-k) >= 0 and (j-k) >= 0]

    return [couleur_direction1,couleur_direction2,couleur_direction3,couleur_direction4,couleur_direction5,couleur_direction6,couleur_direction7,couleur_direction8],[index_direction1,index_direction2,index_direction3,index_direction4,index_direction5,index_direction6,index_direction7,index_direction8]


# NOTE : hommes et femmes inversés ! -> les commentaires contredisent le code

# pour des commentaires contraires au code
def mariage_stable(FC,HC):
    n = len(FC) # nombre de couples à former = nbr d'hommes = nbr de femmes
    Couples = [[f] for f in range(n)]
    celibataire = -1
    Hommes_etat = [ celibataire for h in range(n)] # i = num de l'épouse
    F_Celibataires = list(range(n)) # on crée la liste des célibataires composée d'entiers à partir de 0
    while len(F_Celibataires) > 0 :
        for i in F_Celibataires: # on parcourt la liste des hommes célibataires
            h_pref = FC[i][0] # on nomme la femme préférée de l'homme i
            FC[i].pop(0)     # on enlève la femme du classement de Hi, il peut plus demander
            if Hommes_etat[h_pref] == celibataire :
                Hommes_etat[h_pref] = i
                F_Celibataires.remove(i) # l'homme n'est plus célibataire
            else : 
                epouse = Hommes_etat[h_pref]
                if HC[h_pref][i] > HC[h_pref][epouse] :# premier arrivé, premier servi, CRITIQUE
                    Hommes_etat[h_pref] = i
                    F_Celibataires.append(epouse) # le mari devient célibataire = DIVORCE
                    F_Celibataires.remove(i) # l'homme n'est plus célibataire
    mari = 0
    for epouse in Hommes_etat:
        Couples[epouse].append(mari)
        mari += 1
    return Couples
    

# NOTE : intervalles pas fermés-ouverts, besoin d'une fonction auxiliaire,
# test (di, dj) != (0, 0).
# retourner de façon prématurée.

# tectonik. pour le nom de la fonction test
def test_case(s,i,j,t):
    changement = False 
    for di in range(-1,2):
        for dj in range(-1,2): #on parcours les cases aux alentours de la case étudiée pour chercher les contraintes
            if (0 <= i+di <= 5) and (0 <= j+dj <= 5) and (di != 0 or dj !=0) and t[i][j] != 0 and s[i+di][j+dj] == t[i][j]: 
                t.remove(s[i][j])  #apres le test des contraintes on met a jour la liste des possibilites
                changement = True  #la fonction permet aussi de savoir s'il y a eu un changement 
    return changement
    
# mauvais commentaires, return t, range(6)    
def remplacement(s,t): 
    '''remplace dans grille les cases a une seule possibilite'''
    b=0
    for i in range(6):
        for j in range(6):
            if len(t[i][j]) == 1 and t[i][j] != [0]:
                b=t[i][j]  # b est une variable qui nous permet de passer de la liste a une valeur a la valeur seule
                s[i][j] = b[0] # la case dans la grille prend la seule valeur possiblet
                t[i][j] = [0]  
    return t 

# NOTE : code illisible parce que les variables sont trop longues. if Verif == True...

# kandyCrush
def echanger_deux_bonbons(G,coord1,coord2):
    Verif=False
    #Initialisation de la matrice secondaire
    n,p=np.shape(G)
    W=np.array(G)
    #On intervertit les positions de coord1 et coord2
    W[coord1[0],coord1[1]],W[coord2[0],coord2[1]]=W[coord2[0],coord2[1]],W[coord1[0],coord1[1]]
     #On recherche les combinaisons horizontales et verticales éventuellement créées par l'inversion des deux bonbons.
    if coord1[0]==coord2[0] and abs(coord1[1]-coord2[1])==1:#On vérifie si les deux bonbons échangés sont sur la même ligne et côte à côte
        Combinaison=reperer_alignements_horizontaux(W) + reperer_alignements_verticaux(W) 
    elif coord1[1]==coord2[1] and abs(coord1[0]-coord2[0])==1:#Si les deux bonbons échangés sont sur la même colonne et côte à côte
        Combinaison=reperer_alignements_horizontaux(W) + reperer_alignements_verticaux(W)
    #Si les conditions de position ne sont pas vérifiées, on considère que la liste de combinaison est nulle (afin d'éviter tout échange non permis par les règles)
    else:
        Combinaison=[]
    if len(Combinaison) != 0:
            Verif=True
    #Si la condition est vérifiée (ie : l'un des deux bonbons du couple est engagé dans une combinaison horizontale et/ou verticale), on autorise le déplacement
    if Verif==True:
        G[coord1[0],coord1[1]],G[coord2[0],coord2[1]]=G[coord2[0],coord2[1]],G[coord1[0],coord1[1]]
    return Verif # permet de savoir si une modification de la grille a été effectuée

# NOTE : bon exemple pour un nettoyage en direct.

def remplacer_alignements_par_cases_vides(G,score):
    nouveau_score= score
    liste_alignements_horizontaux= reperer_alignements_horizontaux(G)
    liste_alignements_verticaux=reperer_alignements_verticaux(G)
    for k in range(len(liste_alignements_horizontaux)):
        for x in range(len(liste_alignements_horizontaux[k])):
            if len(liste_alignements_horizontaux[k])==3:
                n=liste_alignements_horizontaux[k][x][0]#n prend la valeur de la coordonnée de ligne
                p=liste_alignements_horizontaux[k][x][1]#p prend la valeur de la coordonnée de colonne
                G[n,p]= VIDE
                nouveau_score += 20 #20 points par bonbon impliqué dans une combinaison de trois, donc 60 points sont obtenus pour un alignement de trois 
            elif len(liste_alignements_horizontaux[k])==4:
                G[liste_alignements_horizontaux[k][0][0],liste_alignements_horizontaux[k][0][1]]= SUPCOLONNE #le premier élément d'un alignement de 4 est remplacé par un bonbon spécial
                n=liste_alignements_horizontaux[k][x][0]
                p=liste_alignements_horizontaux[k][x][1] 
                G[n,p]= VIDE
                nouveau_score += 30
            else :
                G[liste_alignements_horizontaux[k][0][0],liste_alignements_horizontaux[k][0][1]]= BOMBE
                n=liste_alignements_horizontaux[k][x][0]
                p=liste_alignements_horizontaux[k][x][1] 
                G[n,p]= VIDE
                nouveau_score += 40
    for k in range(len(liste_alignements_verticaux)):
        for x in range(len(liste_alignements_verticaux[k])):
            if len(liste_alignements_verticaux[k])==3:
                n=liste_alignements_verticaux[k][x][0]#n prend la valeur de la coordonnée de ligne
                p=liste_alignements_verticaux[k][x][1]#p prend la valeur de la coordonnée de colonne
                G[n,p]= VIDE
                nouveau_score += 20
            elif len(liste_alignements_verticaux[k])==4:
                G[liste_alignements_verticaux[k][0][0],liste_alignements_verticaux[k][0][1]]= SUPLIGNE #le premier élément d'un alignement de 4 est remplacé par un bonbon spécial
                n=liste_alignements_verticaux[k][x][0]
                p=liste_alignements_verticaux[k][x][1] 
                G[n,p]= VIDE
                nouveau_score += 30
            else :
                G[liste_alignements_verticaux[k][0][0],liste_alignements_verticaux[k][0][1]]= BOMBE
                n=liste_alignements_verticaux[k][x][0]
                p=liste_alignements_verticaux[k][x][1] 
                G[n,p]= VIDE
                nouveau_score += 40
    return nouveau_score
    
    
# NOTE : faire des boucles et utiliser des directions...

def matriceinitiale():
    '''Création de la matrice d'incidence initiale'''
    T=np.zeros((81,81))
    for N in range(81):
        [i,j]=inversef(N)
        if i==1: #S'il est sur la première ligne
            if j==1: #S'il est sur la première colonne
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
            elif j==17: #S'il est sur la dernière colonne
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
            else:
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
        
        elif i==17: #S'il est sur la dernière ligne
            if j==1: #S'il est sur la première colonne
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
            elif j==17: #S'il est sur la dernière colonne
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
            else:
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus       
                
        else: #S'il n'est ni sur la première ni sur la dernière ligne
            if j==1: #S'il est sur la première colonne
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
            elif j==17: #S'il est sur la dernière colonne
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
            else: 
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
    return T
    

# NOTE : berk berk berk (principe de moindre surprise)
# pour l'algo de tri
def tri_insertion(LISTE):
    """Fonction qui trie la liste selon la méthode du tri par insertion
                                            et renvoie la liste triée"""
    for k in range (1,len(LISTE)):
        LISTE=insere(LISTE,k,cherche_place(LISTE,k))
    return LISTE
    
    
def choix_pers(POPULATION,jour,CHANGEAVIS):
    """Sélection des personnes dont l'opinion évolue sur une unité de temps"""
    PERSCHANGEAVIS = []
    NBCHANGEAVIS=[] #Comporte le nombre de fois où les personnes changent d'avis
    # On choisit aléatoirement le nombre de personnes dont l'opinion va évoluer
    nbperschoisi = rd.randint(0,POPULATION.shape[0])
    # On travaille sur la copie pour ne pas perdre certaines données
    COPIE=[]
    if jour == 0:
        for k in range (0,nbperschoisi):
            pers = rd.randint(0,POPULATION.shape[0]-1)
            while pers in PERSCHANGEAVIS:
                #Pour ne pas choisir la même personne 
                pers = rd.randint(0,POPULATION.shape[0]-1)
            PERSCHANGEAVIS.append(pers)
            CHANGEAVIS[pers][1] += 1
    elif jour ==1:
        #Rentre les personnes n'ayant pas été influencés au temps 0
        COPIE=[x for x in CHANGEAVIS if x[1]!=1]
        if nbperschoisi >len(COPIE):
            for k in range(0,nbperschoisi-len(COPIE)):
                x = rd.choice([x for x in CHANGEAVIS if x[1]==1])
                while x in COPIE:
                    x = rd.choice([x for x in CHANGEAVIS if x[1]==1])       
                COPIE.append(x)
        #Choix aléatoire des personnes dont l'opinion va changer      
        for k in range (0,nbperschoisi):
            pers = rd.choice(COPIE)[0]
            while pers in PERSCHANGEAVIS:
                pers = rd.choice(COPIE)[0]
               
            PERSCHANGEAVIS.append(pers)
            CHANGEAVIS[pers][1]+=1                      
    else: 
        pers_a_enlever = POPULATION.shape[0]-nbperschoisi
        COPIE = copy.deepcopy(CHANGEAVIS)
        conditionfin = 0  
        compteurmax = 0 # Compte le nombre de max dans la liste AVIS 

        COPIE = tri_insertion(COPIE) 
        for k in range (len(COPIE)):
            NBCHANGEAVIS.append(COPIE[k][1])
        
        while pers_a_enlever>0:
            """On place dans COPIE les personnes qui peuvent
                                             potentiellement changer d'avis"""
            for k in range (0,len(NBCHANGEAVIS)):
                if NBCHANGEAVIS[k]==NBCHANGEAVIS[-1]:
                    #Nombre de fois où le max est répété
                    conditionfin+=1
                    compteurmax+=1
            if conditionfin<pers_a_enlever:
                for k in range (0, compteurmax):
                    NBCHANGEAVIS.remove(NBCHANGEAVIS[-1])
                    COPIE.remove(COPIE[-1])
                    #Correspondance des indices entre AVIS et COPIE
                    pers_a_enlever -=1
                compteurmax=0
                
            else:
                position_premier_max=len(NBCHANGEAVIS)-compteurmax
                for k in range (0,pers_a_enlever):
                    indice= rd.randint(position_premier_max,len(NBCHANGEAVIS)-1)
                    NBCHANGEAVIS.pop (indice)
                    COPIE.pop(indice)
                    pers_a_enlever -=1
                    
        
        for k in range (0,nbperschoisi):
            PERSCHANGEAVIS.append(COPIE[k][0])
            CHANGEAVIS[COPIE[k][0]][1]+=1           
    return(PERSCHANGEAVIS, CHANGEAVIS) 
    
    
# NOTE : il y avait aussi une fonction matrice_4... -> PIECEABOUGER devrait être un paramètre.
# on ne sait pas comment les pièces sont choisies.
# abscisses et ordonnées dans les matrices...

def matrice_2():

    """ Utilise la fonction matrice_1 pour créer un plateau de jeu.

    Modifie les positions initiales de certaines pièces pour créer un second plateau de départ

    Retourne un tableau numpy """

    plateau = matrice_1() 

    PIECEABOUGER = [[7, 5], [5, 7], [5, 11], [7, 13], [11, 13], [13, 11], [13, 7], [11, 5]]

    for i in(PIECEABOUGER):

        abscisse = i[0]

        ordonnee = i[1]

        plateau[abscisse - 1, ordonnee], plateau[abscisse, ordonnee - 1] = plateau[abscisse, ordonnee - 1], plateau[abscisse - 1, ordonnee]

        plateau[abscisse, ordonnee + 1], plateau[abscisse + 1, ordonnee] = plateau[abscisse + 1, ordonnee], plateau[abscisse, ordonnee + 1]

    return plateau


def matrice_3():

    """ Utilise la fonction matrice_2 pour créer un plateau de jeu.

    Modifie les positions initiales de certaines pièces pour créer un troisième plateau de départ

    Retourne un tableau numpy """

    plateau = matrice_2()

    PIECEABOUGER = [[3, 5], [5, 3], [3, 13], [5, 15], [13, 15], [15, 13], [15, 5], [13, 3]]

    for i in(PIECEABOUGER):

        abscisse = i[0]

        ordonnee = i[1]

        plateau[abscisse - 1, ordonnee], plateau[abscisse, ordonnee - 1] = plateau[abscisse, ordonnee - 1], plateau[abscisse - 1, ordonnee]

        plateau[abscisse, ordonnee + 1], plateau[abscisse + 1, ordonnee] = plateau[abscisse + 1, ordonnee], plateau[abscisse, ordonnee + 1]

    return plateau



# NOTE : nom de fonction. Qu'est-ce qui est testé ? Que renvoie la fonction ?
# Elle dessine et ne renvoie rien.
# Lignes vides partout : ne peut pas tenir sur un écran.
def test():
    probasubstitution = 0.000001
    probadeletion = 0.000001
    probaaddition = 0.000001
    sequence=copy.deepcopy(sequenceARNpm)

    L1=[]

    L2=[]

    MoyenneHydropathie=[]

    MoyenneHydropathieref=[]

    (rangcodonstartref, rangcodonstopref, sequenceARNmref)=maturation(sequence)

    (Proteineref, Hydropathieref, Rangacideamineref)=traduction(sequenceARNmref)

    MoyenneHydropathieref=moyenne_hydropathie(Hydropathieref)

    Helicealpharef=structures_secondaires(MoyenneHydropathieref)

    (sequencemutee, nombresubstitution, nombredeletion,nombreaddition)=mutation(sequence,probasubstitution,probadeletion,probaaddition)

    (rangcodonstart, rangcodonstop, sequenceARNm)=maturation(sequencemutee)

    if sequenceARNm==('Proteine non fonctionnelle'):

        Limite=[0]*len(Rangacideamineref)

        plt.plot(Rangacideamineref,MoyenneHydropathieref, label="reference")

        plt.plot(Rangacideamineref,Limite, 'k')

        plt.xlabel("Rang acide amine")

        plt.ylabel("Indice d'hydropathie")

        plt.title("Profil d'hydropathie de la Proteine reference"+"\n"+"Sequence mutee non traduite")

        plt.legend() 

        plt.show()

    else:         

        (Proteine, Hydropathie, Rangacideamine)=traduction(sequenceARNm)

        MoyenneHydropathie=moyenne_hydropathie(Hydropathie)

        Helicealpha=structures_secondaires(MoyenneHydropathie)

        if len(Rangacideamineref)>len(Rangacideamine):

            for k in range(len(Rangacideamineref)-len(Rangacideamine)):

                MoyenneHydropathie.append(0)

            Rangacideamine=Rangacideamineref

        else:

            for k in range(len(Rangacideamine)-len(Rangacideamineref)):

               MoyenneHydropathieref.append(0)

        Limite=[0]*len(Rangacideamine)



        plt.plot(Rangacideamine,MoyenneHydropathieref, label="reference",linewidth=3.5)

        plt.plot(Rangacideamine,MoyenneHydropathie,label="mutee")

        plt.plot(Rangacideamine,Limite, 'k')

        plt.xlabel("Rang acide amine")

        plt.ylabel("Indice d'hydropathie")

        if Helicealpha!=Helicealpharef:

            plt.title("Profil d'hydropathie des proteine de reference et mutee(non fonctionnelle)")

        else:

            plt.title("Profil d'hydropathie des poteines de reference et mutee(fonctionnelle)")

        plt.legend()    

        plt.show()

# juste pour le choix du nom
def mortenfonctiondedegats(Virusbis,p,p2,degats):

# pour le return Liste et pour pour le ==True    
def removepion (LISTEPION,PION) :
    """Supprime une configuration du pion dans la listepion"""
    for k in range (0, len(LISTEPION)):
        if np.all(PION == LISTEPION[k]) == True :
            LISTEPION.pop(k)
            return (LISTEPION)
            
            
# pour les if imbriqué et le nom de test
def verifplace2 (PLATEAUJOUEUR, PLATEAUADV1, PLATEAUADV2, PLATEAUADV3, PION,
                 PLACEPOSSIBLE):
    """Cette fonction rassemble toutes les conditions de vérifications
    des règles"""
    
    PLACEVERIFIEE = []
    ligne, colonne = PION.shape
    for A in PLACEPOSSIBLE :
        for i in range (0, ligne) :
            for j in range (0, colonne) :
                if verifplacegrandplateau (PLATEAUJOUEUR, PION, A, i, j) == True :
                    if verifpetitplateau (PLATEAUJOUEUR, PION, A, i, j) == True :
                        if verifplateauadv (PLATEAUJOUEUR, PLATEAUADV1,
                                            PLATEAUADV2, PLATEAUADV3, PION, A,
                                            i, j) == True :
                            PLACEVERIFIEE.append([A, [i, j]])
    return(PLACEVERIFIEE)
