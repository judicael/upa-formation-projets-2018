def placesverifiees(plj, pla1, pla2, pla3, p, pp):
    """Retourne les places, parmi celles de pp, où règles sont bien respectées.f
    plj, pla1, pla2 et pla3 représentent respectivement les plateaux
    du joueur, du premier adversaire, du second et du troisième, p est
    la position du pion.""" 
    v = [] # les places vérifiées
    ni, nj = p.shape
    for c in pp:
        for i in range (0, ni) :
            for j in range (0, nj) :
                if verifplacegrandplateau (pj, p, c, i, j) and \
                   verifpetitplateau (plj, p, c, i, j) and \
                   verifplateauadv(plj, pa1, pa2g pa3, p, c):
                            v.append([A, [i, j]])
    return v
