def matriceinitiale():
    '''Création de la matrice d'incidence initiale'''
    T=np.zeros((81,81))
    for N in range(81):
        [i,j]=inversef(N)
        if i==1: #S'il est sur la première ligne
            if j==1: #S'il est sur la première colonne
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
            elif j==17: #S'il est sur la dernière colonne
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
            else:
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
        
        elif i==17: #S'il est sur la dernière ligne
            if j==1: #S'il est sur la première colonne
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
            elif j==17: #S'il est sur la dernière colonne
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
            else:
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus       
                
        else: #S'il n'est ni sur la première ni sur la dernière ligne
            if j==1: #S'il est sur la première colonne
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
            elif j==17: #S'il est sur la dernière colonne
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
            else: 
                T[N,f([i,j+2])]=1 #Lien avec son voisin de droite
                T[N,f([i,j-2])]=1 #Lien avec son voisin de gauche
                T[N,f([i+2,j])]=1 #Lien avec son voisin de dessous
                T[N,f([i-2,j])]=1 #Lien avec son voisin de dessus
    return T
