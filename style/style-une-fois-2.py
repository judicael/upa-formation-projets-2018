TAILLE_PLATEAU = 8
DIRECTIONS = [(di, dj) for di in (-1,0,1)
                       for dj in (-1,0,1) if (di, dj) != (0,0)]

def est_valide(i, j):
    """Dit si la case de coordonnées (i, j) existe"""
    return 0 <= i < TAILLE_PLATEAU and 0 <= j < TAILLE_PLATEAU

def directions(i, j, p):
    dirs = []
    for di, dj in DIRECTIONS:
        c = [(i + k*di, j + k*dj) for k in range(1, 2*n)]
        dirs.append([p[i, j] for i,j in c if est_valide(i, j)])
    return dirs
