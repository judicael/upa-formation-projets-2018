PIECEABOUGER = [ [7,  5], [ 5,  7], [ 5, 11], [ 7, 13],
                [11, 13], [13, 11], [13,  7], [11,  5]]

def matrice_2():
    p = matrice_1() 
    for c in PIECEABOUGER:
        i, j = c
        p[i-1, j], p[i, j-1] = p[i, j-1], p[i-1, j]
        p[i, j+1], p[i+1, j] = p[i+1, j], p[i, j+1]
    return p

