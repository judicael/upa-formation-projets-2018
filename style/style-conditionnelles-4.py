def verifplace2 (PLATEAUJOUEUR, PLATEAUADV1, PLATEAUADV2, PLATEAUADV3, PION,
                 PLACEPOSSIBLE):
    """Cette fonction rassemble toutes les conditions de vérifications
    des règles"""
    
    PLACEVERIFIEE = []
    ligne, colonne = PION.shape
    for A in PLACEPOSSIBLE :
        for i in range (0, ligne) :
            for j in range (0, colonne) :
                if verifplacegrandplateau (PLATEAUJOUEUR, PION, A, i, j) == True :
                    if verifpetitplateau (PLATEAUJOUEUR, PION, A, i, j) == True :
                        if verifplateauadv (PLATEAUJOUEUR, PLATEAUADV1,
                                            PLATEAUADV2, PLATEAUADV3, PION, A,
                                            i, j) == True :
                            PLACEVERIFIEE.append([A, [i, j]])
    return(PLACEVERIFIEE)
