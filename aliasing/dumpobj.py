#! /usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os

MODULE = 1000000

def objid(obj):
    return id(obj) % MODULE
    
def name(obj):
    return "adr{0:X}".format(objid(obj))

class Pile:
    def __init__(self, bindings):
        self.bindings = list(bindings)
    def contents(self):
        return [('PILE', None)] + self.bindings


def obj_contents(obj):
    """Renvoie elts où elts est une liste de couples d' éléments
    constituant obj, chacun étant représenté comme un label et
    l'objet pointé. Le nom où l'objet pointé peut être None.
    Le premier couple est le couple (nom du type de l'objet, None)"""
    m = getattr(obj, 'contents', None)
    if callable(m):
        return m()
    t = type(obj)
    elts = [("#{}".format(objid(obj)), None)]
    if t == int:
        return elts + [('ENTIER', None), (obj, None)]
    elif t == list:
        return elts + [('TABLEAU', None)] + [(None, v) for v in obj]
    elif t == dict:
        return elts + [('DICT', None)] + list(obj.items())
    elif t == str:
        return elts + [('CHAÎNE', None), (obj, None)]
    elif obj is None:
      n = 'None'
    else:
      # on ne sait pas traiter pour l'instant
      assert False
    return elts
    
def dump_chunk(obj, c=sys.stdout):
    """Sort sur le canal c (stdout par défaut) la représentation
    au format .dot de la portion de mémoire représentant l'objet obj
    (n'affiche pas les objets pointés par obj).
    """
    n = name(obj)
    elts = obj_contents(obj)
    labels = []
    for (l, o) in elts:
        if l is None:
            labels.append('')
        else:
            labels.append('{}'.format(l))
    label = "|".join(
      ["<f{0}> {1}".format(k, s) for (k, s) in enumerate(labels)])
    c.write(' {n}[label="{label}"];\n'.format(n=n,label=label))
    for (k, (l, ref)) in enumerate(elts):
        if ref is not None:
            c.write(' {n}:f{k} -> {dest}:f0\n'.format(n=n,k=k,dest=name(ref)))

def dump_obj(objs):
    """Retourne un dictionnaire contenant l'ensemble des (i, o)
    pour lesquels  o est un objet accessible depuis les éléments de 
    objs et i est son id"""
    stack = objs
    found = {}
    while stack != []:
      obj = stack.pop()
      i = id(obj)
      if i in found:
        continue
      found[i] = obj
      elts = obj_contents(obj)
      stack += [ref for (i, ref) in elts if ref is not None]
    return found

def dump_graph(objs, f):
    """Sort sur le fichier f le graphe des objets obj au format .dot"""
    d = dump_obj(objs)
    with open(f, 'w') as c:
      c.write("digraph ObjectGraph {\n")
      c.write('graph[rankdir="LR",margin=0];\n')
      c.write("node[shape=record];\n")
      for o in d.values():
          dump_chunk(o, c)
      c.write("}\n")

def dump_pdf(objs, f):
    dump_graph(objs, f)
    os.system('dot -O -Tpdf ' + f)

