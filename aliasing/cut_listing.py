#! /usr/bin/python3

import os
import argparse

# états de l'automate
SKIP=0
WRITE=1

def cutfile(src, destdir, suf):
    with open(src) as fd: lignes = fd.readlines()
    os.chdir(destdir)
    state = SKIP
    fd = None # descripteur sur lequel écrire
    for s in lignes:
        if state == SKIP and s[:2] == '#b':
            f = s[2:].strip() + suf
            fd = open(f, 'w')
            state = WRITE
        elif state == SKIP:
            pass
        elif state == WRITE and s[:2] == '#e':
            fd.close()
            # os.system('pygmentize -f latex -o {} {}'.format(f+'.tex',f))
            state = SKIP
        elif state == WRITE:
            fd.write(s)
        else:
            assert False


parser = argparse.ArgumentParser(
    description = 'Coupe un listing python en morceaux')
parser.add_argument('listing', metavar='f', help='Fichier a découper')
parser.add_argument('-o', metavar='d', dest='dst', default='.',
                        help='Répertoire destination')
parser.add_argument('-s', metavar='suffix', dest='suf', default='',
                        help='Suffixe à ajouter aux noms de fichiers destinations')

args = parser.parse_args()

cutfile(args.listing, args.dst, args.suf)
