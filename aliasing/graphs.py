#! /usr/bin/python3
# -*- coding: utf-8 -*-
from dumpobj import dump_pdf, Pile

x = (
#b 123soleil
[ 51090942171709440000,
  'bonjour tout le monde',
  [42, 17]
  ]
#e 123soleil
    )
dump_pdf([x], '123soleil.dot')

#b dictexample
a = 123
b = 1789
t = ['hello', 'world']
#e dictexample

dump_pdf([Pile([('a',a),('b',b), ('t',t)])], 'dictexample.dot')

#b aliasing
u = t
#e
dump_pdf([Pile([('a',a),('b',b), ('t',t), ('u',u)])], 'aliasing.dot')

#b conseq-aliasing
u[0] = 'bye!'
#e
dump_pdf([Pile([('a',a),('b',b), ('t',t), ('u',u)])], 'conseq-aliasing.dot')

#b changer-velo
# Vélo : [ couleur, diam_roue_arr, retroviseur ]
peugeot = [ 'bleu', 30, False ]
azub = [ 'rouge', 26, True ]
monvelo = peugeot # en 1987
monvelo = azub # en 2009
monvelo[2] = False # en 2010
#e

#b changer-objet
x = [11, 22, 33]
y = [x, 42]
x.append(44)
#e
dump_pdf([Pile([('x', x), ('y', y)])], 'changer-objet.dot')

#b changer-variable
x = [11, 22, 33]
y = [x, 42]
x = [7, 8]
#e
dump_pdf([Pile([('x', x), ('y', y)])], 'changer-variable.dot')

#b exo1
x = 24
y = x
x += 3
#e
dump_pdf([Pile([('x', y), ('y', y)])], 'exo1.dot')
dump_pdf([Pile([('x', x), ('y', y)])], 'exo1bis.dot')

#b exo2
x = [1, 2, 3]
y = x
x += [7, 8]
#e
x = [1, 2, 3]
y = x
dump_pdf([Pile([('x', x), ('y', y)])], 'exo2.dot')
x += [7, 8]
dump_pdf([Pile([('x', x), ('y', y)])], 'exo2bis.dot')

#b exo3
x = 'hello'
y = x
x += ' world'
#e
x = 'hello'
y = x
dump_pdf([Pile([('x', x), ('y', y)])], 'exo3.dot')
x += ' world'
dump_pdf([Pile([('x', x), ('y', y)])], 'exo3bis.dot')

#b copy
def copie(t):
    u = [0] * len(t)
    for i in range(len(t)):
        u[i] = t[i]
    return u
        
t = [1, 2, 3]
v = copie(t)
#e

def copie(t):
    v = []
    dump_pdf([Pile([('t', t), ('v', v)])], 'copy.dot')
    for x in t:
        v.append(x)
    dump_pdf([Pile([('t', t), ('v', v)])], 'copybis.dot')
    return v
t = [ 11, 22, 33 ]
m = copie(t)
dump_pdf([Pile([('t', t), ('m', m)])], 'copyter.dot')


#b copy2
u = [[1, 2, 3], [4, 5, 6]]
v = u.copy()
w = v
#e
dump_pdf([Pile([('u', u), ('v', v), ('w', w)])], 'copy2.dot')

#b copy3
u[0] = [7, 8, 9]
u[1][0] = 17
#e
dump_pdf([Pile([('u', u), ('v', v), ('w', w)])], 'copy3.dot')

from copy import deepcopy
#b deepcopy
u = [[1, 2, 3], [4, 5, 6]]
v = deepcopy(u)
w = v
#e
dump_pdf([Pile([('u', u), ('v', v), ('w', w)])], 'deepcopy.dot')

#b swap
def swap(u, i, j):
    u[i], u[j] = u[j], u[i]

t = [ 11, 22, 33 ]
swap(t, 0, 1)
#e
def swap(u, i, j):
    dump_pdf([Pile([('t',t), ('u',u),('i',i), ('j',j)])], 'swap.dot')
    u[i], u[j] = u[j], u[i]
    return

t = [ 11, 22, 33]
swap(t, 0, 1)


#b dictexample2
a = b
#e dictexample2
dump_pdf([{'a':a,'b':b, 't':t}], 'dictexample2.dot')
#b dictexample3
a = b + 1
#e dictexample3
dump_pdf([{'a':a,'b':b, 't':t}], 'dictexample3.dot')
#b dictexample4
a = b + 0
#e dictexample4
dump_pdf([{'a':a,'b':b, 't':t}], 'dictexample4.dot')

#b aliasing1
t = [9283, 1009]
u = t
#e aliasing1
dump_pdf([{'t':t,'u':u}], 'aliasing1.dot')
#b aliasing2
t[0] = 42
# u[0] vaut maintenant 42...
#e aliasing2
dump_pdf([{'t':t,'u':u}], 'aliasing2.dot')
#b aliasing3
t = [9283, 1009]
u = [9283, 1009]
#e aliasing3
dump_pdf([{'t':t,'u':u}], 'aliasing3.dot')
#b aliasing3bis
t = [9283, 1009]
u = t[:]
#e aliasing3bis
dump_pdf([{'t':t,'u':u}], 'aliasing3bis.dot')
#b aliasing4
# matrice nulle 4*3
A = [[0]*3]*4
#e aliasing4
dump_pdf([A], 'aliasing4.dot')
#b aliasing5


A[0][2] = 42
#e aliasing5
dump_pdf([A], 'aliasing5.dot')
#b aliasing6
A = [None]*4
#e aliasing6
dump_pdf([A], 'aliasing6.dot')
#b aliasing7
for i in range(4):
  A[i] = [0]*3
#e aliasing7
dump_pdf([A], 'aliasing7.dot')
#b aliasing9
u = [0, 1, 2]
v = [3, 4, u]
u[2] = v
#e aliasing9
dump_pdf([u, v], 'aliasing9.dot')
#b gc1
t = [0, 1, 2]
#e gc1
dump_pdf([{'t':t}], 'gc1.dot')
#b gc2
t = 42
#e gc2
dump_pdf([{'t':t}], 'gc2.dot')

# on sauve l'état de la pile à deux endroits différents :
def f(t):
    dump_pdf([Pile([('u',u), ('t',t)])], 'appel1.dot')
    t[0] += 2
    dump_pdf([Pile([('u',u), ('t',t)])], 'appel1bis.dot')

u = [11, 22, 33]
f(u)
dump_pdf([Pile([('u',u)])], 'appel1ter.dot')
#b appel1
def f(t):
    t[0] += 2

u = [11, 22, 33]
f(u)
#e

# on sauve l'état de la pile à deux endroits différents :
def f(t):
    dump_pdf([Pile([('u',u), ('t',t)])], 'appel2.dot')
    t += 2
    dump_pdf([Pile([('u',u), ('t',t)])], 'appel2bis.dot')
    return t

u = 11
v = f(u)
dump_pdf([Pile([('u',u), ('v',v)])], 'appel2ter.dot')
#b appel2
def f(t):
    t = t + 2
    return t

u = 11
v = f(u)
#e

# on sauve l'état de la pile à deux endroits différents :
def f(t):
    dump_pdf([Pile([('u',u), ('t',t)])], 'appel3.dot')
    t[0] += 2
    dump_pdf([Pile([('u',u), ('t',t)])], 'appel3bis.dot')
    return t

u = [11, 22, 33]
u = f(u)
dump_pdf([Pile([('u',u)])], 'appel3ter.dot')
#b appel3
def f(t):
    t[0] += 2
    return t # là, on cherche les ennuis...

u = [11, 22, 33]
u = f(u)
#e

# on sauve l'état de la pile à deux endroits différents :
def f(t):
    dump_pdf([Pile([('u',u), ('t',t)])], 'appel4.dot')
    t[0] += 2
    dump_pdf([Pile([('u',u), ('t',t)])], 'appel4bis.dot')
    return t

u = [11, 22, 33]
v = f(u)
dump_pdf([Pile([('u',u), ('v',v)])], 'appel4ter.dot')
#b appel4
def f(t):
    t[0] += 2
    return t # là, on cherche les ennuis...

u = [11, 22, 33]
v = f(u)
#e

#b appel5
def f(t):
    m = t
    m[0] += 2
    return m

u = [11, 22, 33]
v = f(u)
#e

def f(t):
    m = t
    dump_pdf([Pile([('u',u), ('t',t), ('m', m)])], 'appel5.dot')
    m[0] += 2
    dump_pdf([Pile([('u',u), ('t',t), ('m', m)])], 'appel5bis.dot')
    return m

u = [11, 22, 33]
v = f(u)
dump_pdf([Pile([('u',u), ('v',v)])], 'appel5ter.dot')

#b appel6
def f(t):
    m = t.copy()
    m[0] += 2
    return m

u = [11, 22, 33]
v = f(u)
#e

def f(t):
    m = t.copy()
    dump_pdf([Pile([('u',u), ('t',t), ('m', m)])], 'appel6.dot')
    m[0] += 2
    dump_pdf([Pile([('u',u), ('t',t), ('m', m)])], 'appel6bis.dot')
    return m

u = [11, 22, 33]
v = f(u)
dump_pdf([Pile([('u',u), ('v',v)])], 'appel6ter.dot')

M = [[0] * 3] * 4
dump_pdf([Pile([('M',M)])], 'mat.dot')
M[0][1] = 17
dump_pdf([Pile([('M',M)])], 'mat2.dot')

