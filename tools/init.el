(require 'ox-latex)
(setq org-latex-listings 'minted
      org-latex-minted-options '(("mathescape" "true")))

